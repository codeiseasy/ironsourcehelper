package com.codeiseasy.ads.mediation.ironsource.listener

interface IronSourceInitializationListener {
    fun onInitializationCompleted()
}