package com.codeiseasy.ads.mediation.ironsource.util

object IronSourceLoadingIndicator {
    val loadingAnimationType = arrayOf(
        "SquareSpinIndicator",
        "LineScalePulseOutRapidIndicator",
        "BallClipRotatePulseIndicator",
        "BallSpinFadeLoaderIndicator",
        "PacmanIndicator",
        "BallClipRotatePulseIndicator",
        "LineScalePartyIndicator"
    )
}