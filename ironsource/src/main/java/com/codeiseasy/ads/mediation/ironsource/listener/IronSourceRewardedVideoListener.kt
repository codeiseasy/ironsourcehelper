package com.codeiseasy.ads.mediation.ironsource.listener

import com.codeiseasy.ads.mediation.ironsource.format.IronSourcePlacement

interface IronSourceRewardedVideoListener  {
    fun onRewardedVideoAdOpened(){}

    fun onRewardedVideoAdClosed(){}

    fun onRewardedVideoAvailabilityChanged(availability: Boolean){}

    fun onRewardedVideoAdStarted(){}

    fun onRewardedVideoAdEnded(){}

    fun onRewardedVideoAdRewarded(placement: IronSourcePlacement?){}

    fun onRewardedVideoAdShowFailed(error: String?){}

    fun onRewardedVideoAdClicked(placement: IronSourcePlacement?){}
}