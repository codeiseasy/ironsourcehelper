package com.codeiseasy.ads.mediation.ironsource

import android.app.Activity
import android.graphics.Color
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.ColorRes
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import com.codeiseasy.ads.mediation.ironsource.dialog.IronSourceLoadingDialog
import com.codeiseasy.ads.mediation.ironsource.format.*
import com.codeiseasy.ads.mediation.ironsource.listener.*
import com.codeiseasy.ads.mediation.ironsource.prefs.IronSourcePreferences
import com.codeiseasy.ads.mediation.ironsource.util.IronSourceCheckNetwork
import com.codeiseasy.ads.mediation.ironsource.util.IronSourceDebugLog
import com.codeiseasy.ads.mediation.ironsource.util.IronSourceLoadingIndicator
import com.codeiseasy.ads.mediation.ironsource.util.IronSourceTextUtils
import com.ironsource.mediationsdk.IronSource
import com.ironsource.mediationsdk.impressionData.ImpressionDataListener
import com.ironsource.mediationsdk.integration.IntegrationHelper
import com.ironsource.mediationsdk.logger.IronSourceError
import com.ironsource.mediationsdk.model.Placement
import com.ironsource.mediationsdk.sdk.InterstitialListener
import com.ironsource.mediationsdk.sdk.OfferwallListener
import com.ironsource.mediationsdk.sdk.RewardedVideoListener
import com.wang.avi.AVLoadingIndicatorView
import java.util.*
import kotlin.collections.ArrayList

import com.codeiseasy.ads.ironsource.R
import com.google.android.gms.ads.*
import com.google.android.gms.ads.initialization.InitializationStatus
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener
import com.ironsource.mediationsdk.ISBannerSize
import com.ironsource.mediationsdk.IronSourceBannerLayout
import com.ironsource.mediationsdk.sdk.BannerListener


class IronSourceHelper {
    companion object {
        private val TAG = IronSourceHelper::class.java.canonicalName
    }
    private var mActivity: Activity? = null
    private var mAppKey: String? = null
    private var mAdvertiserId: String? = null
    private var mBannerPlacementName: String = "DefaultBanner"
    private var mInterstitialPlacementName: String = "DefaultInterstitial"
    private var mRewardedVideoPlacementName: String? = "DefaultRewardedVideo"
    private var mOfferWallPlacementName: String? = "DefaultOfferWall"
    private var mAdMobNativeUnitId: String? = null

    private var isTestMode: Boolean = false
    private var isValidateIntegration: Boolean = false
    private var isInterstitialLoaded: Boolean = false
    private var isRewardedVideoLoaded: Boolean = false
    private var isOfferWallLoaded: Boolean = false

    private var mAdUnits: ArrayList<IronSource.AD_UNIT> = ArrayList()

    private var mLoadingDialogIronSource: IronSourceLoadingDialog? = null
    private var mAnimationAccentColor: Int = android.R.color.holo_orange_dark
    private var isLoadAnimationBeforeLoadAd: Boolean = true

    private var mSharedPreferences: IronSourcePreferences? = null
    private var mBannerSize: IronSourceBannerAdSizes? = null
    private var mBannerAdSize: ISBannerSize = ISBannerSize.BANNER

    private var mIronSourceInitListener: IronSourceInitializationListener? = null
    private var mIronSourceOfferWallListener: IronSourceOfferWallListener? = null

    private var mNativeView: AdMobNativeView? = null
    private var mBannerAdView: IronSourceBannerLayout? = null

    constructor(activity: Activity?) {
        this.mActivity = activity
        this.mSharedPreferences = IronSourcePreferences(activity)
        this.makeLoadingDialog()
    }

    private fun makeLoadingDialog() {
        var loadingView =
            LayoutInflater.from(mActivity).inflate(R.layout.ironsource_loading_layout, null)
        var avLoadingIndicatorView =
            loadingView.findViewById<AVLoadingIndicatorView>(R.id.indicator_view)
        avLoadingIndicatorView.setIndicatorColor(
            ContextCompat.getColor(
                mActivity!!,
                mAnimationAccentColor
            )
        )
        avLoadingIndicatorView.setIndicator(
            IronSourceLoadingIndicator.loadingAnimationType[Random().nextInt(
                IronSourceLoadingIndicator.loadingAnimationType.size
            )]
        )

        var linearLayout = LinearLayout(mActivity)
        var textView = TextView(mActivity)

        var linearParams = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        linearParams.gravity = Gravity.CENTER
        linearLayout.layoutParams = linearParams
        linearLayout.orientation = LinearLayout.VERTICAL
        linearLayout.gravity = Gravity.CENTER

        var textParams = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        textParams.topMargin = 10
        textView.layoutParams = textParams
        textView.text = "Loading Ad..."
        textView.setTextColor(Color.WHITE)
        textView.setShadowLayer(2f, -1f, -1f, Color.GRAY)
        textView.textSize = 18f
        textView.isAllCaps = true

        linearLayout.addView(loadingView)
        linearLayout.addView(textView)

        this.mLoadingDialogIronSource = IronSourceLoadingDialog.init(mActivity)
            .cancelable(false)
            .backgroundColor(android.R.color.white)
            .view(linearLayout)
            .build()
    }

    fun build(): IronSourceHelper {
        this.initialize()
        return this
    }

    private fun initialize() {
        if (IronSourceTextUtils.isEmpty(this.mAppKey))
            throw IllegalStateException("The ad app key must be set on initialization SDK before load ads is called."
            )

        mAdvertiserId = IronSource.getAdvertiserId(mActivity)
        // we're using an advertisingId as the 'userId'

        //Network Connectivity Status
        IronSource.shouldTrackNetworkState(mActivity, true)

        //The integrationHelper is used to validate the integration. Remove the integrationHelper before going live!
        if (isValidateIntegration) {
            IntegrationHelper.validateIntegration(mActivity)
        }

        // set the IronSource offerwall listener
        IronSource.setOfferwallListener(mOfferWallListener)

        // set client side callbacks for the offerwall
        //SupersonicConfig.getConfigObj().clientSideCallbacks = true

        // add impression data listener
        IronSource.addImpressionDataListener(mImpressionDataListener)

        // set the IronSource user id
        if (mAdvertiserId != null) {
            IronSource.setUserId(mAdvertiserId)
        }
        // init the IronSource SDK
        IronSource.init(mActivity, this.mAppKey, *mAdUnits.toTypedArray())

        if (isTestMode) {
            IronSource.setAdaptersDebug(isTestMode)
        }
    }

    fun setAppKey(key: String?) {
        this.mAppKey = key
    }

    fun setBannerPlacementName(tag: String?) {
        this.mBannerPlacementName = tag.toString()
    }

    fun setInterstitialPlacementName(placementName: String?) {
        this.mInterstitialPlacementName = placementName.toString()
    }

    fun setRewardedVideoPlacementName(placementName: String?) {
        this.mRewardedVideoPlacementName = placementName.toString()
    }

    fun setOfferWallPlacementName(placementName: String?) {
        this.mOfferWallPlacementName = placementName.toString()
    }

    fun setAdMobNativeUnitId(unitId: String?) {
        this.mAdMobNativeUnitId = unitId
    }

    fun setBannerSize(adSize: IronSourceBannerAdSizes) {
        setAdSize(adSize)
        this.mBannerSize = adSize
    }

    fun setTestMode(debug: Boolean) {
        this.isTestMode = debug
    }

    fun setValidateIntegration(validate: Boolean) {
        this.isValidateIntegration = validate
    }

    fun setAdUnits(unitIds: ArrayList<IronSourceAdUnits>) {
        if (unitIds != null && unitIds.isNotEmpty()) {
            for (ids in unitIds) {
                when (ids) {
                    IronSourceAdUnits.BANNER -> mAdUnits.add(IronSource.AD_UNIT.BANNER)
                    IronSourceAdUnits.INTERSTITIAL -> mAdUnits.add(IronSource.AD_UNIT.INTERSTITIAL)
                    IronSourceAdUnits.REWARDED_VIDEO -> mAdUnits.add(IronSource.AD_UNIT.REWARDED_VIDEO)
                    IronSourceAdUnits.OFFERWALL -> mAdUnits.add(IronSource.AD_UNIT.OFFERWALL)
                }
            }
        }
    }

    fun setLoadAnimationBeforeLoadAd(anim: Boolean) {
        this.isLoadAnimationBeforeLoadAd = anim
    }

    fun setLoadAnimationBeforeLoadAdAccentColor(@ColorRes color: Int) {
        this.mAnimationAccentColor = color
    }

    fun addListener(listener: IronSourceInitializationListener?) {
        this.mIronSourceInitListener = listener
    }

    fun loadBanner(bannerView: IronSourceBannerView?) {
        this.setInitializeBannerAd(bannerView, null)
    }

    fun loadBanner(bannerView: IronSourceBannerView?, listener: IronSourceBannerListener?) {
        this.setInitializeBannerAd(bannerView, listener)
    }

    private fun setInitializeBannerAd(bannerView: IronSourceBannerView?, listener: IronSourceBannerListener?) {
        if (IronSourceTextUtils.isEmpty(this.mBannerPlacementName)) throw IllegalStateException("The ad placement tag must be set on banner ad before loadAd is called.")
        mBannerAdView = bannerView!!.getBannerAdView()
        bannerView.setPlacementName(this.mBannerPlacementName)
        bannerView.setAdSize(mBannerSize)
        bannerView.addListener(listener)
        bannerView.loadAd(mActivity)
    }

    fun loadBanner(bannerView: FrameLayout?) {
        this.setInitializeBannerAd(bannerView, null)
    }

    fun loadBanner(bannerView: FrameLayout?, listener: IronSourceBannerListener?) {
        this.setInitializeBannerAd(bannerView, listener)
    }

    private fun setInitializeBannerAd(mAdContainer: FrameLayout?, listener: IronSourceBannerListener?) {
        if (IronSourceTextUtils.isEmpty(this.mBannerPlacementName)) throw IllegalStateException("The ad placement tag must be set on banner ad before loadAd is called.")
        mBannerAdView = IronSource.createBanner(this.mActivity, mBannerAdSize)
        mBannerAdView?.bannerListener = BannerAdCallback(listener)
        val layoutParams = FrameLayout.LayoutParams(
            FrameLayout.LayoutParams.MATCH_PARENT,
            FrameLayout.LayoutParams.WRAP_CONTENT
        )
        mBannerAdView?.let { nonNullBannerAdView ->
            mAdContainer?.addView(nonNullBannerAdView, 0, layoutParams)
            IronSource.loadBanner(nonNullBannerAdView, mBannerPlacementName)
        }
    }

    private fun setAdSize(adSize: IronSourceBannerAdSizes) {
        Log.d("BANNER_SIZE", adSize.toString())
        when (adSize) {
            IronSourceBannerAdSizes.BANNER -> this.mBannerAdSize = ISBannerSize.BANNER
            IronSourceBannerAdSizes.SMART -> this.mBannerAdSize = ISBannerSize.SMART
            IronSourceBannerAdSizes.RECTANGLE -> this.mBannerAdSize = ISBannerSize.RECTANGLE
            IronSourceBannerAdSizes.LARGE -> this.mBannerAdSize = ISBannerSize.LARGE
        }
    }

    fun loadNative(nativeView: AdMobNativeView?) {
        setInitializeNativeAd(nativeView, null)
    }

    fun loadNative(nativeView: AdMobNativeView?, listener: AdMobNativeListener?) {
        setInitializeNativeAd(nativeView, listener)
    }

    private fun setInitializeNativeAd(nativeView: AdMobNativeView?, listener: AdMobNativeListener?) {
        if (IronSourceTextUtils.isEmpty(this.mAdMobNativeUnitId)) throw IllegalStateException("The ad unitID must be set on Native ad before loadAd is called.")
        /*MobileAds.setRequestConfiguration(RequestConfiguration.Builder()
            .setTestDeviceIds(listOf("6A1303EAC79560D8560A8C518E2AAE3B"))
            .build())*/
        MobileAds.initialize(mActivity) {
            nativeView?.setUnitId(mAdMobNativeUnitId)
            nativeView?.addListener(listener)
            nativeView?.loadAd(mActivity)
        }
    }

    fun loadNative(adContainer: FrameLayout?) {
        setInitializeNativeAd(adContainer, null, AdMobNativeView.TemplateType.MEDIUM)
    }

    fun loadNative(adContainer: FrameLayout?, template: AdMobNativeView.TemplateType) {
        setInitializeNativeAd(adContainer, null, template)
    }

    fun loadNative(adContainer: FrameLayout?, listener: AdMobNativeListener?) {
        setInitializeNativeAd(adContainer, listener, AdMobNativeView.TemplateType.MEDIUM)
    }

    fun loadNative(adContainer: FrameLayout?, listener: AdMobNativeListener?, template: AdMobNativeView.TemplateType) {
        setInitializeNativeAd(adContainer, listener, template)
    }

    private fun setInitializeNativeAd(adContainer: FrameLayout?, listener: AdMobNativeListener?, template: AdMobNativeView.TemplateType) {
        MobileAds.initialize(mActivity!!.applicationContext, InitializationCompleteListener())
        MobileAds.setRequestConfiguration(MobileAds.getRequestConfiguration()
            .toBuilder()
            .setTagForChildDirectedTreatment(RequestConfiguration.TAG_FOR_CHILD_DIRECTED_TREATMENT_TRUE)
            .setMaxAdContentRating(RequestConfiguration.MAX_AD_CONTENT_RATING_G)
            .build())

        val nativeViewParams = FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.WRAP_CONTENT)
        mNativeView = AdMobNativeView(mActivity!!.applicationContext, template)
        mNativeView?.setUnitId(mAdMobNativeUnitId)
        mNativeView?.addListener(listener)
        mNativeView?.loadAd(mActivity)
        adContainer?.addView(mNativeView, nativeViewParams)
    }

    class InitializationCompleteListener : OnInitializationCompleteListener {
        override fun onInitializationComplete(initializationStatus: InitializationStatus) {}
    }

    fun loadInterstitialAd() {
        this.setInitializeInterstitialAd(null, -1, null)
    }

    fun loadInterstitialAd(view: View?, count: Int) {
        this.setInitializeInterstitialAd(view!!.id.toString(), count, null)
    }

    fun loadInterstitialAd(key: String?, count: Int) {
        this.setInitializeInterstitialAd(key.toString(), count, null)
    }

    fun loadInterstitialAd(listener: IronSourceInterstitialListener?) {
        this.setInitializeInterstitialAd(null, -1, listener)
    }

    fun loadInterstitialAd(view: View?, count: Int, listener: IronSourceInterstitialListener?) {
        this.setInitializeInterstitialAd(view!!.id.toString(), count, listener)
    }

    fun loadInterstitialAd(key: String?, count: Int, listener: IronSourceInterstitialListener?) {
        this.setInitializeInterstitialAd(key.toString(), count, listener)
    }

    private fun setInitializeInterstitialAd(
        key: String?,
        count: Int,
        listener: IronSourceInterstitialListener?
    ) {
        if (IronSourceTextUtils.isEmpty(this.mInterstitialPlacementName)) throw IllegalStateException(
            "The ad placementName must be set on interstitial ad before loadAd is called."
        )
        //this.mIronSourceInterstitialListener = listener
        if (count != -1 && count > 0) {
            var prefKey = "interstitial_ad_" + key.toString()
            var totalCount = doCount(prefKey)
            if (this.mSharedPreferences!!.getAdCountPref(prefKey) == count) {
                this.mSharedPreferences?.setAdCountPref(prefKey, 1)
                this.setPrepareInterstitialAd(listener)
            } else {
                this.mSharedPreferences?.setAdCountPref(prefKey, totalCount)
                listener?.onInterstitialAdClosed()
            }
        } else {
            this.setPrepareInterstitialAd(listener)
        }
    }

    private fun setPrepareInterstitialAd(listener: IronSourceInterstitialListener?) {
        if (IronSourceCheckNetwork.getInstance(mActivity).isOnline) {
            runOnUiThread(showLoading)
            // set the interstitial listener
            IronSource.setInterstitialListener(InterstitialAdCallback(listener))
            if (!isInterstitialLoaded) {
                IronSource.loadInterstitial()
            } else {
                // check if interstitial is available
                if (IronSource.isInterstitialReady()) {
                    //show the interstitial
                    IronSource.showInterstitial(mInterstitialPlacementName)
                    return
                }
                IronSource.loadInterstitial()
                listener?.onInterstitialAdLoadFailed(getString(R.string.ironsource_no_inter_found))
                runOnUiThread(hideLoading)
            }
        } else {
            runOnUiThread(hideLoading)
            listener?.onInterstitialAdLoadFailed(getString(R.string.ironsource_no_internet_access))
        }
    }

    fun loadRewardedVideoAd() {
        this.setInitializeRewardedVideoAd(null, -1, null)
    }

    fun loadRewardedVideoAd(listener: IronSourceRewardedVideoListener?) {
        this.setInitializeRewardedVideoAd(null, -1, listener)
    }

    fun loadRewardedVideoAd(key: String?, count: Int) {
        this.setInitializeRewardedVideoAd(key, count, null)
    }

    fun loadRewardedVideoAd(view: View?, count: Int) {
        this.setInitializeRewardedVideoAd(view!!.id.toString(), count, null)
    }

    fun loadRewardedVideoAd(key: String?, count: Int, listener: IronSourceRewardedVideoListener?) {
        this.setInitializeRewardedVideoAd(key, count, listener)
    }

    fun loadRewardedVideoAd(view: View?, count: Int, listener: IronSourceRewardedVideoListener?) {
        this.setInitializeRewardedVideoAd(view!!.id.toString(), count, listener)
    }

    private fun setInitializeRewardedVideoAd(key: String?, count: Int, listener: IronSourceRewardedVideoListener?) {
        if (IronSourceTextUtils.isEmpty(this.mBannerPlacementName)) throw IllegalStateException("The ad placement tag must be set on rewarded video ad before loadAd is called.")
        if (count != -1 && count > 0) {
            var prefKey = "rewarded_video_ad_" + key.toString()
            var totalCount = doCount(prefKey)
            if (this.mSharedPreferences!!.getAdCountPref(prefKey) == count) {
                this.mSharedPreferences?.setAdCountPref(prefKey, 1)
                this.setPrepareRewardedVideo(listener)
            } else {
                this.mSharedPreferences?.setAdCountPref(prefKey, totalCount)
                listener?.onRewardedVideoAdClosed()
            }
        } else {
            this.setPrepareRewardedVideo(listener)
        }
    }

    private fun setPrepareRewardedVideo(listener: IronSourceRewardedVideoListener?) {
        if (IronSourceCheckNetwork.getInstance(mActivity).isOnline) {
            runOnUiThread(showLoading)
            // set the IronSource rewarded video listener
            IronSource.setRewardedVideoListener(RewardedVideoAdCallback(listener))
            // check if video is available
            if (IronSource.isRewardedVideoAvailable()) {
                //show rewarded video
                IronSource.showRewardedVideo()
                return
            }
            runOnUiThread(hideLoading)
            listener?.onRewardedVideoAdShowFailed(getString(R.string.ironsource_no_reward_found))
        } else {
            runOnUiThread(hideLoading)
            listener?.onRewardedVideoAdShowFailed(getString(R.string.ironsource_no_internet_access))
        }
    }

    class Builder {
        private var activity: Activity? = null
        private var mAppKey: String? = null
        private var mBannerPlacementName: String = "DefaultBanner"
        private var mInterstitialPlacementName: String = "DefaultInterstitial"
        private var mRewardedVideoPlacementName: String? = "DefaultRewardedVideo"
        private var mOfferWallPlacementName: String? = "DefaultOfferWall"
        private var mAdMobNativeUnitId: String? = "ca-app-pub-3940256099942544/2247696110"
        private var mBannerSize: IronSourceBannerAdSizes = IronSourceBannerAdSizes.BANNER
        private var isTestMode: Boolean = false
        private var isValidateIntegration: Boolean = false
        private var mAdUnits: ArrayList<IronSourceAdUnits> = ArrayList()

        private var mAnimationAccentColor: Int = android.R.color.holo_orange_dark
        private var isLoadAnimationBeforeLoadAd: Boolean = true
        private var mIronSourceInitListener: IronSourceInitializationListener? = null

        fun setActivity(activity: Activity?): Builder {
            this.activity = activity
            return this
        }

        fun setAppKey(key: String?): Builder {
            this.mAppKey = key
            return this
        }

        fun setBannerPlacementName(placementName: String?): Builder {
            this.mBannerPlacementName = placementName.toString()
            return this
        }

        fun setInterstitialPlacementName(placementName: String?): Builder {
            this.mInterstitialPlacementName = placementName.toString()
            return this
        }

        fun setRewardedVideoPlacementName(placementName: String?): Builder {
            this.mRewardedVideoPlacementName = placementName.toString()
            return this
        }

        fun setOfferWallPlacementName(placementName: String?): Builder {
            this.mOfferWallPlacementName = placementName.toString()
            return this
        }

        fun setAdMobNativeUnitId(unitId: String?): Builder {
            this.mAdMobNativeUnitId = unitId.toString()
            return this
        }

        fun setBannerSize(adSize: IronSourceBannerAdSizes): Builder {
            this.mBannerSize = adSize
            return this
        }

        fun setTestMode(debug: Boolean): Builder {
            this.isTestMode = debug
            return this
        }

        fun setValidateIntegration(validate: Boolean): Builder {
            this.isValidateIntegration = validate
            return this
        }

        fun setAdUnits(unitIds: ArrayList<IronSourceAdUnits>): Builder {
            this.mAdUnits = unitIds
            return this
        }

        fun setLoadAnimationBeforeLoadAd(anim: Boolean): Builder {
            this.isLoadAnimationBeforeLoadAd = anim
            return this
        }

        fun setLoadAnimationBeforeLoadAdAccentColor(@ColorRes color: Int): Builder {
            this.mAnimationAccentColor = color
            return this
        }

        fun addListener(listener: IronSourceInitializationListener?): Builder {
            this.mIronSourceInitListener = listener
            return this
        }

        fun build(): IronSourceHelper {
            val helper = IronSourceHelper(this.activity)
            helper.setAppKey(this.mAppKey)
            helper.setBannerPlacementName(this.mBannerPlacementName)
            helper.setInterstitialPlacementName(this.mInterstitialPlacementName)
            helper.setRewardedVideoPlacementName(this.mRewardedVideoPlacementName)
            helper.setOfferWallPlacementName(this.mOfferWallPlacementName)
            helper.setAdMobNativeUnitId(this.mAdMobNativeUnitId)
            helper.setBannerSize(this.mBannerSize)
            helper.setTestMode(this.isTestMode)
            helper.setValidateIntegration(this.isValidateIntegration)
            helper.setAdUnits(this.mAdUnits)
            helper.setLoadAnimationBeforeLoadAd(this.isLoadAnimationBeforeLoadAd)
            helper.setLoadAnimationBeforeLoadAdAccentColor(this.mAnimationAccentColor)
            helper.addListener(this.mIronSourceInitListener)
            return helper.build()
        }
    }

    // Begin ImpressionData Callback
    private val mImpressionDataListener = ImpressionDataListener { p0 ->
        IronSourceDebugLog.d(TAG, p0.toString())
    }
    // End ImpressionData Callback


    // Begin Banner Callback
    inner class BannerAdCallback(private val listener: IronSourceBannerListener?) : BannerListener {
        override fun onBannerAdLoaded() {
            listener?.onBannerAdLoaded()
        }

        override fun onBannerAdLoadFailed(p0: IronSourceError?) {
            listener?.onBannerAdLoadFailed(p0!!.errorMessage)
        }

        override fun onBannerAdClicked() {
            listener?.onBannerAdClicked()
        }

        override fun onBannerAdScreenPresented() {
            listener?.onBannerAdScreenPresented()
        }

        override fun onBannerAdScreenDismissed() {
            listener?.onBannerAdScreenDismissed()
        }

        override fun onBannerAdLeftApplication() {
            listener?.onBannerAdLeftApplication()
        }
    }
    // End Banner Callback

    // Begin Interstitial Callback
    inner class InterstitialAdCallback(private val listener: IronSourceInterstitialListener?) : InterstitialListener {
        override fun onInterstitialAdReady() {
            runOnUiThread(hideLoading)
            if (!isInterstitialLoaded){
                IronSource.showInterstitial(mInterstitialPlacementName)
            }
            isInterstitialLoaded = true
            listener?.onInterstitialAdReady()
        }

        override fun onInterstitialAdOpened() {
            listener?.onInterstitialAdOpened()
        }

        override fun onInterstitialAdClicked() {
            listener?.onInterstitialAdClicked()
        }

        override fun onInterstitialAdClosed() {
            IronSource.loadInterstitial()
            listener?.onInterstitialAdClosed()
        }

        override fun onInterstitialAdLoadFailed(p0: IronSourceError?) {
            runOnUiThread(hideLoading)
            listener?.onInterstitialAdLoadFailed("" + p0!!.errorMessage)
        }

        override fun onInterstitialAdShowFailed(p0: IronSourceError?) {
            runOnUiThread(hideLoading)
            listener?.onInterstitialAdShowFailed("" + p0!!.errorMessage)
        }

        override fun onInterstitialAdShowSucceeded() {
            listener?.onInterstitialAdShowSucceeded()
        }
    }
    // End Interstitial Callback

    // Begin Rewarded video Callback
    inner class RewardedVideoAdCallback(private val listener: IronSourceRewardedVideoListener?) : RewardedVideoListener {
        override fun onRewardedVideoAdOpened() {
            listener?.onRewardedVideoAdOpened()
        }

        override fun onRewardedVideoAdClosed() {
            listener?.onRewardedVideoAdClosed()
        }

        override fun onRewardedVideoAvailabilityChanged(p0: Boolean) {
            listener?.onRewardedVideoAvailabilityChanged(p0)
        }

        override fun onRewardedVideoAdStarted() {
            runOnUiThread(hideLoading)
            listener?.onRewardedVideoAdStarted()
        }

        override fun onRewardedVideoAdEnded() {
            listener?.onRewardedVideoAdEnded()
        }

        override fun onRewardedVideoAdRewarded(p0: Placement?) {
            listener?.onRewardedVideoAdRewarded(addPlacement(p0))
        }

        override fun onRewardedVideoAdShowFailed(p0: IronSourceError?) {
            runOnUiThread(hideLoading)
            listener?.onRewardedVideoAdShowFailed(p0!!.errorMessage)
        }

        override fun onRewardedVideoAdClicked(p0: Placement?) {
            listener?.onRewardedVideoAdClicked(addPlacement(p0))
        }
    }
    // End Rewarded video Callback

    // Begin Offerwall Callback
    private val mOfferWallListener = object : OfferwallListener {
        override fun onOfferwallAvailable(p0: Boolean) {
            mIronSourceOfferWallListener?.onOfferWallAvailable(p0)
        }

        override fun onOfferwallOpened() {
            mIronSourceOfferWallListener?.onOfferWallOpened()
        }

        override fun onOfferwallClosed() {
            mIronSourceOfferWallListener?.onOfferWallClosed()
        }

        override fun onOfferwallShowFailed(p0: IronSourceError?) {
            mIronSourceOfferWallListener?.onOfferWallShowFailed(p0!!.errorMessage)
        }

        override fun onOfferwallAdCredited(p0: Int, p1: Int, p2: Boolean): Boolean {
            mIronSourceOfferWallListener?.onOfferWallAdCredited(p0, p1, p2)
            return false
        }

        override fun onGetOfferwallCreditsFailed(p0: IronSourceError?) {
            mIronSourceOfferWallListener?.onGetOfferWallCreditsFailed(p0!!.errorMessage)
        }
    }
    // Begin Offerwall Callback

    private fun addPlacement(p0: Placement?): IronSourcePlacement {
        return IronSourcePlacement(
            p0!!.placementId, p0.placementName,
            p0.isDefault, p0.rewardName, p0.rewardAmount
        )
    }

    private fun runOnUiThread(runnable: Runnable) {
        mActivity?.runOnUiThread(runnable)
    }

    private fun doCount(key: String?): Int {
        return this.mSharedPreferences!!.getAdCountPref(key) + 1
    }

    private fun getString(@StringRes res: Int): String? {
        return mActivity!!.resources.getString(res)
    }

    private val showLoading = Runnable {
        if (isLoadAnimationBeforeLoadAd) {
            if (this.mLoadingDialogIronSource != null) {
                this.mLoadingDialogIronSource?.show()
            }
        }
    }

    private val hideLoading = Runnable {
        if (isLoadAnimationBeforeLoadAd) {
            Log.d("LOADING_INDICATOR", "Force Cancel Loading")
            if (this.mLoadingDialogIronSource != null) {
                this.mLoadingDialogIronSource?.hide()
            }
        }
    }

    fun destroy() {
        mNativeView?.destroyNativeAd()
        if (mBannerAdView != null) {
            IronSource.destroyBanner(mBannerAdView)
        }
    }

    fun resume() {
        IronSource.onResume(mActivity)
    }

    fun pause() {
        IronSource.onPause(mActivity)
    }
}