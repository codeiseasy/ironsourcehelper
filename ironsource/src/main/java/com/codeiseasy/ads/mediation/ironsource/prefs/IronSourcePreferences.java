package com.codeiseasy.ads.mediation.ironsource.prefs;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by NoThing on 6/12/2018.
 */

public class IronSourcePreferences {
    public SharedPreferences preferences;
    public Context context;

    public IronSourcePreferences(Context context) {
        this.context = context;
    }

    // setupShape Ad Count
    public void setAdCountPref(String key, int value) {
        preferences = context.getSharedPreferences("ad_count_pref", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    // get Ad Count
    public int getAdCountPref(String key){
        return context.getSharedPreferences("ad_count_pref", Context.MODE_PRIVATE).getInt(key, 1);
    }
}
