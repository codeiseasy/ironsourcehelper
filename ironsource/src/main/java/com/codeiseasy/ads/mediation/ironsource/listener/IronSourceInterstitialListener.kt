package com.codeiseasy.ads.mediation.ironsource.listener

interface IronSourceInterstitialListener  {
    fun onInterstitialAdReady() {}

    fun onInterstitialAdLoadFailed(error: String?) {}

    fun onInterstitialAdOpened() {}

    fun onInterstitialAdClosed() {}

    fun onInterstitialAdShowSucceeded() {}

    fun onInterstitialAdShowFailed(error: String?) {}

    fun onInterstitialAdClicked() {}
}