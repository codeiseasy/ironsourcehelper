package com.codeiseasy.ads.mediation.ironsource.format

data class IronSourcePlacement(
    val mPlacementId: Int = 0,
    val mPlacementName: String? = null,
    val mIsDefault: Boolean = false,
    val mRewardName: String? = null,
    val mRewardAmount: Int = 0
)