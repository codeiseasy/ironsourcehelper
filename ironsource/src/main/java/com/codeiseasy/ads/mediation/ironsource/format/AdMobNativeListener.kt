package com.codeiseasy.ads.mediation.ironsource.format

interface AdMobNativeListener {
    open fun onAdClosed() {}
    fun onAdFailedToLoad(error: String?) {}
    fun onAdLeftApplication() {}
    fun onAdOpened() {}
    fun onAdLoaded() {}
    fun onAdClicked() {}
    fun onAdImpression() {}
}