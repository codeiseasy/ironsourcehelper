package com.codeiseasy.ads.mediation.ironsource.listener

interface IronSourceBannerListener {
    fun onBannerAdLoaded(){}

    fun onBannerAdLoadFailed(error: String?){}

    fun onBannerAdClicked(){}

    fun onBannerAdScreenPresented(){}

    fun onBannerAdScreenDismissed(){}

    fun onBannerAdLeftApplication(){}
}