package com.codeiseasy.ads.mediation.ironsource.format

import android.app.Activity
import android.content.Context
import android.os.Build
import android.text.TextUtils
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.annotation.LayoutRes
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import com.codeiseasy.ads.ironsource.R
import com.codeiseasy.ads.mediation.ironsource.util.IronSourceDebugLog
import com.google.android.gms.ads.*
import com.google.android.gms.ads.nativead.NativeAd
import com.google.android.gms.ads.nativead.NativeAdOptions
import com.google.android.gms.ads.nativead.NativeAdView
import com.wang.avi.AVLoadingIndicatorView
import kotlin.random.Random


/**
 * Author: Med Ajaroud
 * Created At: 06/02/2020
 * Github: https://github.com/mrajaroud
 * License: Non-commercial
 */

/** Base class for a native ad template view. *  */
class AdMobNativeView : RelativeLayout {
    private var mActivity: Activity? = null
    private var mTemplateRes = -1
    private var mAdMobNativeListener: AdMobNativeListener? = null

    private var isLoadAnimationBeforeLoadAd: Boolean = true
    private var mAnimationAccentColor = 0

    //private var mNativeAdContainer: FrameLayout? = null
    private var mNativeAdView: NativeAdView? = null
    private var mNativeAd: NativeAd? = null
    private var mNativeAdLoadedListener: NativeAd.OnNativeAdLoadedListener? = null
    private var mLayoutInflater: LayoutInflater? = null
    private var mLoadingIndicatorView: AVLoadingIndicatorView? = null
    private var mLoadingView: View? = null
    private var mUnitId: String? = null
    private var isLoadAdError: Boolean = false
    private var mTemplateType: TemplateType = TemplateType.MEDIUM

    constructor(context: Context?, @LayoutRes layoutRes: Int) : super(context) {
        this.mTemplateRes = layoutRes
        this.initView(context, null)
    }

    constructor(context: Context?, template: TemplateType) : super(context) {
        this.mTemplateType = template
        this.initView(context, null)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        this.initView(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        this.initView(context, attrs)
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) : super(context, attrs, defStyleAttr, defStyleRes) {
        this.initView(context, attrs)
    }

    fun setUnitId(unitId: String?) {
        this.mUnitId = unitId.toString()
    }

    fun addListener(listener: AdMobNativeListener?) {
        this.mAdMobNativeListener = listener
    }

    private fun initView(context: Context?, attributeSet: AttributeSet?) {
        this.mLayoutInflater = context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        if (attributeSet != null) {
            val attributes = context.theme.obtainStyledAttributes(attributeSet, R.styleable.AdMobNativeView, 0, 0)
            try {
                this.isLoadAnimationBeforeLoadAd = attributes.getBoolean(R.styleable.AdMobNativeView_gnt_is_animation, true)
                this.mAnimationAccentColor = attributes.getColor(R.styleable.AdMobNativeView_gnt_animation_color, ContextCompat.getColor(context, android.R.color.holo_red_light))
                if (attributes.hasValue(R.styleable.AdMobNativeView_gnt_template_type)){
                    initDefaultTemplate(TemplateType.values()[attributes.getInt(R.styleable.AdMobNativeView_gnt_template_type, 0)])
                }
            } finally {
                attributes.recycle()
            }
        } else {
            initDefaultTemplate(mTemplateType)
        }
    }

    enum class TemplateType(val index: Int) {
        SMALL(0), MEDIUM(1), LARGE(2)
    }

    private fun initDefaultTemplate(template: TemplateType) {
        when (template) {
            TemplateType.MEDIUM -> {
                this.mTemplateRes = R.layout.gnt_medium_template
                this.mNativeAdLoadedListener = NativeAdLoadedListener()
                inflate(context, this.mTemplateRes, this)
                initMediumNativeViews()
            }
            TemplateType.LARGE -> {
                this.mTemplateRes = R.layout.gnt_medium2_template
                this.mNativeAdLoadedListener = NativeAdLoadedListener()
                inflate(context, this.mTemplateRes, this)
                initMediumNativeViews()
            }
            else -> {
                this.mTemplateRes = R.layout.gnt_small_template
                this.mNativeAdLoadedListener = NativeAdBannerLoadedListener()
                inflate(context, this.mTemplateRes, this)
                initSmallNativeViews()
            }
        }
    }

    fun loadAd(activity: Activity?) {
        load(activity, -1)
    }

    fun loadAd(activity: Activity?, maxNumberOfAds: Int) {
        load(activity, maxNumberOfAds)
    }

    private fun load(activity: Activity?, maxNumberOfAds: Int) {
        this.mActivity = activity
        try {
            val adLoader = AdLoader.Builder(context, mUnitId.toString())
                .forNativeAd(this.mNativeAdLoadedListener)
                .withNativeAdOptions(
                    NativeAdOptions.Builder()
                        .setVideoOptions(
                            VideoOptions.Builder()
                                .setStartMuted(false)
                                .build()
                        ).setRequestCustomMuteThisAd(false).build()
                ).withAdListener(NativeAdListener())
                .build()
            if (maxNumberOfAds != -1) {
                adLoader.loadAd(AdRequest.Builder().build())
            } else {
                adLoader.loadAds(AdRequest.Builder().build(), maxNumberOfAds)
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun initSmallNativeViews() {
        mNativeAdView = findViewById(R.id.native_view)

        mNativeAdView?.headlineView = findViewById(R.id.ad_headline)
        mNativeAdView?.bodyView = findViewById(R.id.ad_body)
        mNativeAdView?.callToActionView = findViewById(R.id.ad_call_to_action)
        mNativeAdView?.iconView = findViewById(R.id.ad_app_icon)

        //this.mNativeAdContainer = findViewById(R.id.gnt_ad_container)
        this.mLoadingIndicatorView = findViewById(R.id.indicator_view)
        this.mLoadingView = findViewById(R.id.gnt_animation_view)
        this.mLoadingView?.setBackgroundColor(ContextCompat.getColor(context, R.color.gnt_background_color))
        this.mLoadingIndicatorView?.setIndicatorColor(this.mAnimationAccentColor)
        this.mLoadingIndicatorView?.setIndicator(
            loadingAnimationType[Random.nextInt(
                loadingAnimationType.size)])
        this.viewsVisibility()
    }

    private fun initMediumNativeViews() {
        mNativeAdView = findViewById(R.id.native_view)

        mNativeAdView?.mediaView = findViewById(R.id.ad_media)
        mNativeAdView?.headlineView = findViewById(R.id.ad_headline)
        mNativeAdView?.bodyView = findViewById(R.id.ad_body)
        mNativeAdView?.callToActionView = findViewById(R.id.ad_call_to_action)
        mNativeAdView?.iconView = findViewById(R.id.ad_app_icon)
        mNativeAdView?.priceView = findViewById(R.id.ad_price)
        mNativeAdView?.starRatingView = findViewById(R.id.ad_stars)
        mNativeAdView?.storeView = findViewById(R.id.ad_store)
        mNativeAdView?.advertiserView = findViewById(R.id.ad_advertiser)

        //this.mNativeAdContainer = findViewById(R.id.gnt_ad_container)
        this.mLoadingIndicatorView = findViewById(R.id.indicator_view)
        this.mLoadingView = findViewById(R.id.gnt_animation_view)
        this.mLoadingView?.setBackgroundColor(ContextCompat.getColor(context, R.color.gnt_background_color))
        this.mLoadingIndicatorView?.setIndicatorColor(this.mAnimationAccentColor)
        this.mLoadingIndicatorView?.setIndicator(
            loadingAnimationType[Random.nextInt(
                loadingAnimationType.size)])
        this.viewsVisibility()
    }

    inner class NativeAdBannerLoadedListener :
        NativeAd.OnNativeAdLoadedListener {
        override fun onNativeAdLoaded(ad: NativeAd) {
            destroyNativeAd()
            mNativeAd = ad
            //val nativeAdView = mLayoutInflater!!.inflate(R.layout.ad_unified_banner, null as ViewGroup?) as NativeAdView
            try {
                (mNativeAdView!!.headlineView as TextView).text = ad.headline
                (mNativeAdView!!.bodyView as TextView).text = ad.body
                (mNativeAdView!!.callToActionView as Button).text = ad.callToAction
                if (ad.icon == null) {
                    mNativeAdView!!.iconView.visibility = View.GONE
                } else {
                    (mNativeAdView!!.iconView as ImageView).setImageDrawable(ad.icon.drawable)
                    mNativeAdView!!.iconView.visibility = View.VISIBLE
                }
                mNativeAdView?.setNativeAd(ad)

            } catch (unused: Exception) {
            }
            //mNativeAdContainer?.addView(mNativeAdView)
        }
    }

    inner class NativeAdLoadedListener() :
        NativeAd.OnNativeAdLoadedListener {
        override fun onNativeAdLoaded(ad: NativeAd) {
            destroyNativeAd()
            mNativeAd = ad
            try {
                (mNativeAdView!!.headlineView as TextView).text = ad.headline
                (mNativeAdView!!.bodyView as TextView).text = ad.body
                (mNativeAdView!!.callToActionView as Button).text = ad.callToAction

                if (mNativeAdView!!.iconView == null) {
                    (mNativeAdView!!.iconView as ImageView).visibility = View.GONE
                } else {
                    (mNativeAdView!!.iconView as ImageView).setImageDrawable(ad.icon.drawable)
                    mNativeAdView!!.iconView.visibility = View.VISIBLE
                }

                if (mNativeAdView!!.priceView == null) {
                    mNativeAdView!!.priceView?.visibility = View.INVISIBLE
                } else {
                    (mNativeAdView!!.priceView as TextView).text = ad.price
                    mNativeAdView!!.priceView.visibility = View.VISIBLE
                }

                if (mNativeAdView!!.storeView == null) {
                    mNativeAdView!!.storeView?.visibility = View.INVISIBLE
                } else {
                    (mNativeAdView!!.storeView as TextView).text = ad.store
                    mNativeAdView!!.storeView.visibility = View.VISIBLE
                }

                if (mNativeAdView!!.starRatingView == null) {
                    mNativeAdView!!.starRatingView?.visibility = View.INVISIBLE
                } else {
                    (mNativeAdView!!.starRatingView as RatingBar).rating = ad.starRating.toFloat()
                    mNativeAdView!!.starRatingView.visibility = View.VISIBLE
                }

                if (mNativeAdView!!.advertiserView == null) {
                    mNativeAdView!!.advertiserView?.visibility = View.INVISIBLE
                } else {
                    (mNativeAdView!!.advertiserView as TextView).text = ad.advertiser
                    mNativeAdView!!.advertiserView.visibility = View.VISIBLE
                }
                isLoadAnimationBeforeLoadAd = false
                isLoadAdError = false
                runOnUiThread { viewsVisibility() }
                mNativeAdView?.setNativeAd(ad)

            } catch (unused: Exception) {
            }
            //mNativeAdContainer?.addView(mNativeAdView)
        }
    }

    inner class NativeAdListener : AdListener() {
        override fun onAdLoaded() {
            super.onAdLoaded()
            isLoadAnimationBeforeLoadAd = false
            isLoadAdError = false
            runOnUiThread { viewsVisibility() }
            mAdMobNativeListener?.onAdLoaded()
            IronSourceDebugLog.d(TAG, "onAdLoaded()")
        }

        override fun onAdOpened() {
            super.onAdOpened()
            mAdMobNativeListener?.onAdOpened()
            IronSourceDebugLog.d(TAG, "onAdOpened()")
        }

        override fun onAdClosed() {
            super.onAdClosed()
            mAdMobNativeListener?.onAdClosed()
            IronSourceDebugLog.d(TAG, "onAdClosed()")
        }

        override fun onAdClicked() {
            super.onAdClicked()
            mAdMobNativeListener?.onAdClicked()
            IronSourceDebugLog.d(TAG, "onAdClicked()")
        }

        override fun onAdFailedToLoad(error: LoadAdError) {
            super.onAdFailedToLoad(error)
            isLoadAnimationBeforeLoadAd = false
            isLoadAdError = true
            runOnUiThread { viewsVisibility() }
            mAdMobNativeListener?.onAdFailedToLoad(error.message)
            IronSourceDebugLog.d(TAG, "Failed to load native ad: onAdFailedToLoad(${error.message})")
        }

        override fun onAdImpression() {
            super.onAdImpression()
            mAdMobNativeListener?.onAdImpression()
            IronSourceDebugLog.d(TAG, "onAdImpression()")
        }
    }

    private fun viewsVisibility() {
        if (isLoadAnimationBeforeLoadAd) {
            mNativeAdView?.visibility = View.INVISIBLE
            mLoadingView?.visibility = View.VISIBLE
            return
        }
        mLoadingView?.visibility = View.GONE
        mNativeAdView?.visibility = View.VISIBLE
    }

    private fun adHasOnlyStore(nativeAd: NativeAd): Boolean {
        val store = nativeAd.store
        val advertiser = nativeAd.advertiser
        return !TextUtils.isEmpty(store) && TextUtils.isEmpty(advertiser)
    }
    /**
     * To prevent memory leaks, make sure to destroy your ad when you don't need it anymore. This
     * method does not destroy the template view.
     * https://developers.google.com/admob/android/native-unified#destroy_ad
     */
    fun destroyNativeAd() {
        if (this.mNativeAd != null){
            //this.mNativeAdContainer?.removeAllViews()
            this.mNativeAd?.destroy()
        }
    }

    private fun runOnUiThread(runnable: Runnable) {
        mActivity?.runOnUiThread(runnable)
    }

    companion object {
        private val TAG = AdMobNativeView::class.java.canonicalName
        private val loadingAnimationType = arrayOf(
            "SquareSpinIndicator",
            "LineScalePulseOutRapidIndicator",
            "BallClipRotatePulseIndicator",
            "BallSpinFadeLoaderIndicator",
            "PacmanIndicator",
            "BallClipRotatePulseIndicator",
            "LineScalePartyIndicator"
        )
    }
}