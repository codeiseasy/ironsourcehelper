package com.codeiseasy.ads.mediation.ironsource.format

import android.content.Context
import android.graphics.Color
import android.os.Build
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import androidx.annotation.RequiresApi
import com.codeiseasy.ads.ironsource.R
import com.codeiseasy.ads.mediation.ironsource.util.IronSourceLoadingIndicator.loadingAnimationType

import com.wang.avi.AVLoadingIndicatorView
import java.util.*
import android.app.Activity
import android.widget.FrameLayout
import com.codeiseasy.ads.mediation.ironsource.listener.IronSourceBannerListener
import com.codeiseasy.ads.mediation.ironsource.util.IronSourceCheckNetwork
import com.ironsource.mediationsdk.ISBannerSize
import com.ironsource.mediationsdk.IronSource
import com.ironsource.mediationsdk.IronSourceBannerLayout
import com.ironsource.mediationsdk.logger.IronSourceError
import com.ironsource.mediationsdk.sdk.BannerListener

class IronSourceBannerView : LinearLayout {

    private var mActivity: Activity? = null
    private var mAnimationAccentColor: Int = -1
    private var mBackgroundViewColor: Int = Color.WHITE

    private var mAdContainer: FrameLayout? = null
    private var mBannerAdView: IronSourceBannerLayout? = null
    private var mBannerAdSize: ISBannerSize = ISBannerSize.BANNER

    private var mAvLoadingIndicatorView: AVLoadingIndicatorView? = null
    private var mIronSourceBannerListener: IronSourceBannerListener? = null

    private var mPlacementName: String? = null
    private var isAnimation: Boolean = false

    constructor(context: Context?) : super(context!!) {
        initAttrs(context, null, 0, 0)
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context!!, attrs) {
        initAttrs(context, attrs, 0, 0)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context!!,
        attrs,
        defStyleAttr
    ) {
        initAttrs(context, attrs, defStyleAttr, 0)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(
        context: Context?,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) : super(context!!, attrs, defStyleAttr, defStyleRes) {
        initAttrs(context, attrs, defStyleAttr, defStyleRes)
    }

    override fun getOrientation(): Int {
        return HORIZONTAL
    }

    fun setAdView(bannerLayout: IronSourceBannerLayout?) {
        this.mBannerAdView = bannerLayout
    }

    fun setPlacementName(placement: String?) {
        this.mPlacementName = placement
    }

    fun setAdSize(adSize: IronSourceBannerAdSizes?) {
        when (adSize) {
            IronSourceBannerAdSizes.BANNER -> this.mBannerAdSize = ISBannerSize.BANNER
            IronSourceBannerAdSizes.SMART -> this.mBannerAdSize = ISBannerSize.SMART
            IronSourceBannerAdSizes.RECTANGLE -> this.mBannerAdSize = ISBannerSize.RECTANGLE
            IronSourceBannerAdSizes.LARGE -> this.mBannerAdSize = ISBannerSize.LARGE
        }
    }

    fun addListener(listener: IronSourceBannerListener?) {
        this.mIronSourceBannerListener = listener
    }

    fun loadAd(activity: Activity?) {
        this.mActivity = activity
        if (IronSourceCheckNetwork.getInstance(this.mActivity).isOffline) {
            mAvLoadingIndicatorView?.visibility = View.GONE
            mAdContainer?.visibility = View.GONE
            isAnimation = false
            return
        }

        mBannerAdView = IronSource.createBanner(this.mActivity, mBannerAdSize)
        mBannerAdView?.bannerListener = mBannerListener

        val layoutParams = FrameLayout.LayoutParams(
            FrameLayout.LayoutParams.MATCH_PARENT,
            FrameLayout.LayoutParams.WRAP_CONTENT
        )

        mBannerAdView?.let { nonNullBannerAdView ->
            mAdContainer?.removeAllViews()
            mAdContainer?.addView(nonNullBannerAdView, 0, layoutParams)
            if (mPlacementName.isNullOrEmpty()) {
                IronSource.loadBanner(nonNullBannerAdView, "DefaultBanner")
            } else {
                IronSource.loadBanner(nonNullBannerAdView, mPlacementName)
            }
        }
    }

    private fun initAttrs(
        context: Context?,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) {
        val typedArray = context!!.obtainStyledAttributes(
            attrs,
            R.styleable.IronSourceBannerView,
            defStyleAttr,
            defStyleRes
        )
        try {
            isAnimation = typedArray.getBoolean(
                R.styleable.IronSourceBannerView_ironsource_banner_is_animation,
                false
            )
            mAnimationAccentColor = typedArray.getColor(
                R.styleable.IronSourceBannerView_ironsource_banner_animation_accent_color,
                -1
            )
            mBackgroundViewColor = typedArray.getColor(
                R.styleable.IronSourceBannerView_ironsource_banner_background_color,
                -1
            )
        } finally {
            typedArray.recycle()
        }
        setBackgroundColor(mBackgroundViewColor)
        initViews()
    }

    private fun initViews() {
        inflate(context, R.layout.ironsource_banner_layout, this)
        mAdContainer = findViewById(R.id.banner_container)
        mAvLoadingIndicatorView = findViewById(R.id.indicator_view)

        mAvLoadingIndicatorView?.setIndicator(
            loadingAnimationType[Random().nextInt(
                loadingAnimationType.size
            )]
        )
        mAvLoadingIndicatorView?.setIndicatorColor(mAnimationAccentColor)
        viewsVisibility()
    }

    private fun viewsVisibility() {
        if (isAnimation) {
            mAvLoadingIndicatorView?.visibility = View.VISIBLE
            mAdContainer?.visibility = View.INVISIBLE
            return
        }
        mAvLoadingIndicatorView?.visibility = View.GONE
        mAdContainer?.visibility = View.VISIBLE
    }

    fun getBannerAdView() : IronSourceBannerLayout? {
        return mBannerAdView
    }

    fun destroy() {
        if (mBannerAdView != null) {
            IronSource.destroyBanner(mBannerAdView)
        }
    }

    val mBannerListener = object : BannerListener {
        override fun onBannerAdLoaded() {
            mIronSourceBannerListener?.onBannerAdLoaded()
            isAnimation = false
            viewsVisibility()
        }

        override fun onBannerAdLoadFailed(p0: IronSourceError?) {
            mIronSourceBannerListener?.onBannerAdLoadFailed("Ad failed to load: ${p0!!.errorMessage}")
            isAnimation = false
            viewsVisibility()
        }

        override fun onBannerAdClicked() {
            mIronSourceBannerListener?.onBannerAdClicked()
        }

        override fun onBannerAdScreenPresented() {
            mIronSourceBannerListener?.onBannerAdScreenPresented()
        }

        override fun onBannerAdScreenDismissed() {
            mIronSourceBannerListener?.onBannerAdScreenDismissed()
        }

        override fun onBannerAdLeftApplication() {
            mIronSourceBannerListener?.onBannerAdLeftApplication()
        }
    }

    private fun runOnUiThread(runnable: Runnable) {
        mActivity?.runOnUiThread(runnable)
    }
}