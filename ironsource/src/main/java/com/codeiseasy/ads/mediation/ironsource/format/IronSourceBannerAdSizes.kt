package com.codeiseasy.ads.mediation.ironsource.format

enum class IronSourceBannerAdSizes {
    BANNER, SMART, LARGE, RECTANGLE
}