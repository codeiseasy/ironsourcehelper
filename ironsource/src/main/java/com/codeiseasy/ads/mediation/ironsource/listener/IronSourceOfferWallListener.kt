package com.codeiseasy.ads.mediation.ironsource.listener

interface IronSourceOfferWallListener {
    fun onOfferWallAvailable(available: Boolean)

    fun onOfferWallOpened()

    fun onOfferWallShowFailed(error: String?)

    fun onOfferWallAdCredited(var1: Int, var2: Int, var3: Boolean): Boolean

    fun onGetOfferWallCreditsFailed(error: String?)

    fun onOfferWallClosed()
}