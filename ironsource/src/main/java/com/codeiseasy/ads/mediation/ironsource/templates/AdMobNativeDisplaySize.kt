package com.codeiseasy.ads.mediation.ironsource.templates

enum class AdMobNativeDisplaySize {
    SMALL,
    MEDIUM,
    LARGE
}