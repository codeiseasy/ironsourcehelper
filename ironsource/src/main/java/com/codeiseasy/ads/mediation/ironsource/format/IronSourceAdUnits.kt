package com.codeiseasy.ads.mediation.ironsource.format

enum class IronSourceAdUnits(val placement: String) {
    BANNER("banner"),
    INTERSTITIAL("interstitial"),
    REWARDED_VIDEO("rewardedVideo"),
    OFFERWALL("offerwall")
}