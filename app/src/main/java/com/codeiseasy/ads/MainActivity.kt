package com.codeiseasy.ads

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.FrameLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.codeiseasy.ads.mediation.ironsource.IronSourceHelper
import com.codeiseasy.ads.mediation.ironsource.format.AdMobNativeListener
import com.codeiseasy.ads.mediation.ironsource.format.AdMobNativeView
import com.codeiseasy.ads.mediation.ironsource.format.IronSourceAdUnits
import com.codeiseasy.ads.mediation.ironsource.format.IronSourceBannerAdSizes
import com.codeiseasy.ads.mediation.ironsource.listener.IronSourceBannerListener
import com.codeiseasy.ads.mediation.ironsource.listener.IronSourceInterstitialListener
import com.codeiseasy.ads.mediation.ironsource.listener.IronSourceRewardedVideoListener

class MainActivity : AppCompatActivity() {
    private var mIronSourceHelper: IronSourceHelper? = null
    private var mBannerContainer: FrameLayout? = null

    private var mLoadInterstitial: Button? = null
    private var mAdMobNativeView: AdMobNativeView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mLoadInterstitial = findViewById(R.id.load_interstitial)
        mBannerContainer = findViewById(R.id.bannerContainer)
        mAdMobNativeView = findViewById(R.id.nativeContainer)
        //mNativeView = findViewById(R.id.native_view)

        initIronSource()

        mLoadInterstitial?.setOnClickListener {
            /*mIronSourceHelper?.loadRewardedVideoAd(it, 2, object : IronSourceRewardedVideoListener {
                override fun onRewardedVideoAdClosed() {
                    super.onRewardedVideoAdClosed()
                    Toast.makeText(applicationContext, "onRewardedVideoAdClosed()", Toast.LENGTH_LONG).show()
                }

                override fun onRewardedVideoAdShowFailed(error: String?) {
                    super.onRewardedVideoAdShowFailed(error)
                    Toast.makeText(applicationContext, "onRewardedVideoAdShowFailed($error)", Toast.LENGTH_LONG).show()
                }
            })
            return@setOnClickListener*/

            mIronSourceHelper?.loadInterstitialAd(it, 2, object : IronSourceInterstitialListener {
                override fun onInterstitialAdClosed() {
                    super.onInterstitialAdClosed()
                    Toast.makeText(applicationContext, "onInterstitialAdClosed()", Toast.LENGTH_LONG).show()
                }

                override fun onInterstitialAdLoadFailed(error: String?) {
                    super.onInterstitialAdLoadFailed(error)
                    Toast.makeText(applicationContext, "onInterstitialAdLoadFailed($error)", Toast.LENGTH_LONG).show()
                }
            })
        }
    }

    private fun initIronSource() {
        mIronSourceHelper =
            IronSourceHelper.Builder()
                .setActivity(this)
                .setAppKey("100eb5d99")
                .setAdUnits(arrayListOf(
                    IronSourceAdUnits.BANNER,
                    IronSourceAdUnits.INTERSTITIAL,
                    IronSourceAdUnits.REWARDED_VIDEO
                ))
                .setAdMobNativeUnitId("ca-app-pub-3940256099942544/2247696110")
                .setBannerSize(IronSourceBannerAdSizes.BANNER)
                .setTestMode(BuildConfig.DEBUG)
                .setValidateIntegration(BuildConfig.DEBUG)
                .build()

        //mIronSourceHelper?.loadNative(mBannerContainer, AdMobNativeView.TemplateType.SMALL)
        mIronSourceHelper?.loadNative(mAdMobNativeView, object : AdMobNativeListener {
            override fun onAdLoaded() {
                super.onAdLoaded()
                Toast.makeText(applicationContext, "onAdLoaded()", Toast.LENGTH_LONG).show()
            }

            override fun onAdFailedToLoad(error: String?) {
                super.onAdFailedToLoad(error)
                mAdMobNativeView?.visibility = View.GONE
                Toast.makeText(applicationContext, "onAdFailedToLoad($error)", Toast.LENGTH_LONG).show()
            }
        })
        return
        mIronSourceHelper?.loadBanner(mBannerContainer, object : IronSourceBannerListener {
            override fun onBannerAdLoaded() {
                super.onBannerAdLoaded()
                Toast.makeText(applicationContext, "onBannerAdLoaded()", Toast.LENGTH_LONG).show()
            }
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        mIronSourceHelper?.destroy()
        //mNativeView?.destroyNativeAd()
    }

    override fun onResume() {
        super.onResume()
        mIronSourceHelper?.resume()
    }

    override fun onPause() {
        super.onPause()
        mIronSourceHelper?.pause()
    }
}